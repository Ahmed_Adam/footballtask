//
//  leaguesTableViewCell.swift
//  Football-InfoApp
//
//  Created by Adam on 11/18/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit

class leaguesTableViewCell: UITableViewCell {

    @IBOutlet weak var teams: UILabel!
    @IBOutlet weak var games: UILabel!
    @IBOutlet weak var code: UILabel!
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
