//
//  ViewController.swift
//  Football-InfoApp
//
//  Created by Adam on 11/17/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import  RealmSwift

class FootballViewController: UIViewController , UITableViewDelegate , UITableViewDataSource , NVActivityIndicatorViewable{
    
    @IBOutlet weak var tableView: UITableView!
    
    let reachabilityManager = NetworkReachabilityManager()
    var leagues : Football?
    var realmLeagues : [Competition] = []
    var offline = false
    var leagueID : Int?
    var leagueName : String?
    var start : String?
    var end : String?
    var area : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.offline = false
        loading()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        let check = checkNetwork()
        if check {
            getLeagues()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.offline == true{
            let count = self.realmLeagues.count
            return count
        }
        if (self.leagues?.competitions.count) != nil  {
            return self.leagues?.competitions.count ?? 0
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // when no internet connection Load from Realm DB
        if self.offline == true{
            let cell = tableView.dequeueReusableCell(withIdentifier: "LeagueCell") as! leaguesTableViewCell
            cell.name.text = self.realmLeagues[indexPath.row].name
            //            cell.backGround.image = UIImage(data: self.realmImages[indexPath.row].Image! as Data)
            return cell
        }
        
        //  load Characters from API
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeagueCell") as! leaguesTableViewCell
        cell.name.text = self.leagues?.competitions[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.leagueID = self.leagues?.competitions[indexPath.row].id
        self.leagueName = self.leagues?.competitions[indexPath.row].name
        self.start = self.leagues?.competitions[indexPath.row].currentSeason?.startDate
        self.end = self.leagues?.competitions[indexPath.row].currentSeason?.endDate
        //        self.area = self.leagues?.competitions[indexPath.row].area?.name
        self.performSegue(withIdentifier: "ShowLeage", sender: self)
    }
    
    func getLeagues(){
        let url = "competitions"
        WebServices.base.getMethod(requestUrl: url) { (error, success , response ) in
            print(response)
            self.leagues = Football(json: response)
            for league in (self.leagues?.competitions)! {
                RealmManager.shared.addObject(realmObject: league , andCompletion : {
                    (addResult) in
                    print(addResult)
                } )
            }
            self.tableView.reloadData()
            self.stopAnimating()
        }
    }
        
    // Loading
    func loading(){
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "", type: NVActivityIndicatorType.lineSpinFadeLoader)
    }
    
    // send Character Id by segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowLeage" {
            if let leagueViewController = segue.destination as? LeagueViewController {
                leagueViewController.id = self.leagueID
                leagueViewController.leagueName = self.leagueName
                //                leagueViewController.country = self.area
                leagueViewController.end = self.end
                leagueViewController.start = self.start
            }
        }
    }
    
    func checkNetwork()-> Bool {
        reachabilityManager?.startListening()
        if  reachabilityManager?.isReachable == false {
            print("The network is not reachable")
            self.stopAnimating()
            let alert = UIAlertController(title: "Alert", message: " The network is not reachable , please make sure of internet connection " , preferredStyle: UIAlertController.Style.alert)
            self.present(alert, animated: true, completion: nil)
            alert.addAction(UIAlertAction(title: "Close application ", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    exit(0)
                case .cancel:
                    print("cancel")
                case .destructive:
                    print("destructive")
                }}))
            
            alert.addAction(UIAlertAction(title: "wait for connection ... ", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    self.loading()
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 5) {
                        self.stopAnimating()
                        let check = self.checkNetwork()
                        if check {
                            self.getLeagues()
                        }
                    }
                case .cancel:
                    print("cancel")
                case .destructive:
                    print("destructive")
                }}))
            alert.addAction(UIAlertAction(title: " work offline  ", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    self.offline = true
                    let retrivedData = RealmManager.shared.getObjectOf(type: Competition.self)
                    self.realmLeagues = retrivedData
                    self.tableView.reloadData()
                case .cancel:
                    print("cancel")
                case .destructive:
                    print("destructive")
                }}))
            return false
        }
        return true
    }
    
}

