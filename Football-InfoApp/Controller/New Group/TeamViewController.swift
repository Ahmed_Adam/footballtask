//
//  TeamViewController.swift
//  Football-InfoApp
//
//  Created by Adam on 11/17/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import  RealmSwift

class TeamViewController: UIViewController , UITableViewDelegate , UITableViewDataSource , NVActivityIndicatorViewable  {
    
    @IBOutlet weak var image: UIWebView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var shortName: UILabel!
    @IBOutlet weak var venu: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var colors: UILabel!
    @IBOutlet weak var found: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var teamID :Int?
    var team : TeamInfo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loading()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        getTeamData ( id: teamID ?? 0)
    }
    
    func getTeamData ( id:Int){
        let url = "teams/" + "\(id)"
        WebServices.base.getMethod(requestUrl: url) { (error, success , response ) in
            print(response)
            self.team = TeamInfo(json: response)
            self.assignData()
            self.tableView.reloadData()
            self.stopAnimating()
        }
    }
    
    func assignData(){
        let team = self.team
        self.name.text = team?.name
        self.colors.text = team?.clubColors
        self.found.text = "\(team?.founded ?? 0)"
        self.phone.text = team?.phone
        self.shortName.text = team?.shortName
        self.venu.text = team?.venue
        let imageUrl = team?.crestUrl
        let url = URL(string: imageUrl ?? "")
        self.image.loadRequest(URLRequest(url: url!))
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.team?.squad.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "teamCell", for: indexPath) as! SquadTableViewCell
        let player = self.team?.squad[indexPath.row]
        cell.name.text = player?.name
        cell.nationality.text = player?.nationality
//        cell.number.text = String(describing: player?.shirtNumber)
        cell.position.text = player?.position
        
        return cell
    }
    
    func loading(){
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "", type: NVActivityIndicatorType.lineSpinFadeLoader)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
