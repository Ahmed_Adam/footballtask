//
//  TeamsTableViewCell.swift
//  Football-InfoApp
//
//  Created by Adam on 11/20/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit

class TeamsTableViewCell: UITableViewCell {
    
//    @IBOutlet weak var teamImage: UIImageView!
//    @IBOutlet weak var teamImage: UIView!
    @IBOutlet weak var webImage: UIWebView!
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var shortName: UILabel!
    @IBOutlet weak var color: UILabel!
    @IBOutlet weak var Venue: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
