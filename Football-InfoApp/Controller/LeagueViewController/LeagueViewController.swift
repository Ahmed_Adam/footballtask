//
//  LeagueViewController.swift
//  Football-InfoApp
//
//  Created by Adam on 11/17/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import  RealmSwift

class LeagueViewController: UIViewController , UITableViewDataSource , UITableViewDelegate , NVActivityIndicatorViewable {
    
    var id :Int?
    var leagueName:String?
    var league:League?
    var teamID: Int?
    var start:String?
    var end:String?
    
    @IBOutlet weak var noData: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var endDate: UILabel!
    @IBOutlet weak var area: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loading()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        getLeagueData ( id: id ?? 0)
    }
    override func viewDidAppear(_ animated: Bool) {
        self.name.text = leagueName
        self.area.text = "-"
        self.startDate.text = start
        self.endDate.text = end
        if self.league?.teams.count == 0{
            self.tableView.isHidden = true
            self.noData.isHidden = false
        }
        else{
            self.tableView.isHidden = false
            self.noData.isHidden = true
        }
    }
    func getLeagueData ( id:Int){
        let url = "competitions/" + "\(id)" + "/teams"
        WebServices.base.getMethod(requestUrl: url) { (error, success , response ) in
            print(response)
            self.league = League(json: response)
            self.tableView.reloadData()
            self.stopAnimating()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.league?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "teamCell") as! TeamsTableViewCell
        let team = self.league?.teams[indexPath.row]
        cell.teamName.text = team?.name
        cell.shortName.text = team?.shortName
        cell.color.text = team?.clubColors
        cell.Venue.text = team?.venue
        let imageUrl = team?.crestUrl
        let url = URL(string: imageUrl ?? "")
        cell.webImage.loadRequest(URLRequest(url: url!))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.teamID = self.league?.teams[indexPath.row].id
        
        self.performSegue(withIdentifier: "ShowTeamInfo", sender: self)
    }
    
    func loading(){
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "", type: NVActivityIndicatorType.lineSpinFadeLoader)
    }
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "ShowTeamInfo" {
            if let teamViewController = segue.destination as? TeamViewController {
                teamViewController.teamID = self.teamID
            }
        }
    }
    
    
}
