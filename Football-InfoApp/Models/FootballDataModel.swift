//
//  FootballDataModel.swift
//  Football-InfoApp
//
//  Created by Adam on 11/17/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import EVReflection

class Football: Object, EVReflectable {
    @objc dynamic var count: Int = 0
    @objc dynamic var filters: Filters? = nil
    var competitions: List<Competition> = List<Competition>()

    private enum CodingKeys: String, CodingKey {
        case count
        case filters
        // Set JSON Object Key
        case competitions = "competitions"
    }
}

class Competition:  Object, EVReflectable{
    @objc dynamic var id: Int = 0
//    @objc dynamic var area: Area?
    @objc dynamic var name: String?
    @objc dynamic var code: String?
  //  @objc dynamic var emblemURL: String = ""
    @objc dynamic var plan: String = ""
    @objc dynamic var currentSeason: CurrentSeason? = nil
    @objc dynamic var numberOfAvailableSeasons: Int = 0
//    @objc dynamic var lastUpdated: Date? = nil

    enum CodingKeys: String, CodingKey {
        case id,  name, code
        case emblemURL = "emblemUrl"
        case plan, currentSeason, numberOfAvailableSeasons, lastUpdated
    }
    

}

class CurrentSeason:  Object, EVReflectable {
    @objc dynamic var id: Int = 0
    @objc dynamic var startDate = ""
    @objc dynamic var endDate: String = ""
//    @objc dynamic var currentMatchday: Int = 0
//    @objc dynamic var winner: Winner? = nil
}

class Winner: Object, EVReflectable {
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var shortName : String? = ""
    @objc dynamic var vartla: String? = ""
    @objc dynamic var crestURL: String? = ""

    enum CodingKeys: String, CodingKey {
        case id, name, shortName, tla
        case crestURL = "crestUrl"
    }
}

//class Area: Object, EVReflectable {
//    var id: Int = 0
//    var name: String? = ""
//    
//}

class Filters: Object, EVReflectable {
    
    
}
