//
//  League.swift
//  Football-InfoApp
//
//  Created by Adam on 11/17/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import EVReflection


class League:  Object, EVReflectable {
    @objc dynamic var count: Int = 0
    @objc dynamic var filters: Filters?
    @objc dynamic var competition: Competition?
    @objc dynamic var season: Season?
    var teams = List<Team>()
}

class Season:  Object, EVReflectable {
    @objc dynamic var id: Int = 0
    @objc dynamic var startDate, endDate: String?
    @objc dynamic var currentMatchday: Int = 0
    @objc dynamic var winner: String?
}

class Team:  Object, EVReflectable {
    @objc dynamic var id: Int = 0
//    @objc dynamic var area: Area?
    @objc dynamic var name, shortName, tla: String?
    @objc dynamic var crestUrl: String? = ""
    @objc dynamic var address: String?
    @objc dynamic var phone: String?
    @objc dynamic var website: String?
    @objc dynamic var email: String?
    @objc dynamic var founded: Int = 0
    @objc dynamic var clubColors, venue: String?
 //   @objc dynamic var lastUpdated: Date?
    
    enum CodingKeys: String, CodingKey {
        case id, area, name, shortName, tla
        case crestURL = "crestUrl"
        case address, phone, website, email, founded, clubColors, venue
    }
}

