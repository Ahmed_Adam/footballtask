//
//  TeamInfo.swift
//  Football-InfoApp
//
//  Created by Adam on 11/17/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import EVReflection


class TeamInfo: Object, EVReflectable {
    
    @objc dynamic var id: Int = 0
//    @objc dynamic var area: Area?
    var activeCompetitions = List<ActiveCompetition>()
    @objc dynamic var name, shortName, tla: String?
    @objc dynamic var crestUrl: String?
    @objc dynamic var address, phone: String?
    @objc dynamic var website: String?
    @objc dynamic var email: String?
    @objc dynamic var founded: Int = 0
    @objc dynamic var clubColors, venue: String?
    var squad = List<Squad>()
    @objc dynamic var lastUpdated: Date?
    
    enum CodingKeys: String, CodingKey {
        case id,/* area,*/ activeCompetitions, name, shortName, tla
        case crestURL = "crestUrl"
        case address, phone, website, email, founded, clubColors, venue, squad, lastUpdated
    }
}

class ActiveCompetition: Object, EVReflectable {
    @objc dynamic var id: Int = 0
//    @objc dynamic var area:Area? = nil
    @objc dynamic var name :String? = ""
    @objc dynamic var code :String? = ""
    @objc dynamic var plan: String? = ""
//    @objc dynamic var lastUpdated: Date? = nil
}


class Squad: Object, EVReflectable {
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String? = ""
    @objc dynamic var position: String? = ""
//    @objc dynamic var dateOfBirth: Date? = ""
    @objc dynamic var countryOfBirth = ""
    @objc dynamic var nationality: String? = ""
//    @objc dynamic var shirtNumber: Int = 0  
    @objc dynamic var role: String? = ""
}


