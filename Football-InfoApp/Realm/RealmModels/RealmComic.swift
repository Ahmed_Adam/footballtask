////
////  RealmComic.swift
////  Marvel
////
////  Created by Adam on 10/17/18.
////  Copyright © 2018 Adam. All rights reserved.
////
//
//import Foundation
//import  RealmSwift
//
//class RealmFootball: Object {
//    @objc  dynamic var count: Int = 0
//    @objc  dynamic var filters: Filters
//    var items: List<Competition> = List<Competition>()
//    
//
//
//    var realmListItems: [String] {
//        get{
//            return Array(items)
//        }
//        set{
//            safeWrite {
//                items.append(objectsIn: newValue)
//            }
//        }
//        
//    }
//    var realmFilters: Filters {
//        get{
//            return filters
//        }
//        set{
//            safeWrite {
//                returned = newValue
//            }
//        }
//        
//    }
//    var realmCount: Int {
//        get{
//            return count
//        }
//        set{
//            safeWrite {
//                returned = newValue
//            }
//        }
//        
//    }
//}
//
//struct Competition: Object {
//    let id: Int
//    let area: Area
//    let name: String
//    let code: String?
//    let emblemURL: String?
//    let plan: Plan
//    let currentSeason: CurrentSeason?
//    let numberOfAvailableSeasons: Int
//    let lastUpdated: Date
//    
//    enum CodingKeys: String, CodingKey {
//        case id, area, name, code
//        case emblemURL = "emblemUrl"
//        case plan, currentSeason, numberOfAvailableSeasons, lastUpdated
//    }
//}
//
//struct CurrentSeason: Codable {
//    let id: Int
//    let startDate, endDate: String
//    let currentMatchday: Int?
//    let winner: Winner?
//}
//
//struct Winner: Codable {
//    let id: Int
//    let name: String
//    let shortName, tla: String?
//    let crestURL: String?
//    
//    enum CodingKeys: String, CodingKey {
//        case id, name, shortName, tla
//        case crestURL = "crestUrl"
//    }
//}
//
//enum Plan: String, Codable {
//    case tierFour = "TIER_FOUR"
//    case tierOne = "TIER_ONE"
//    case tierThree = "TIER_THREE"
//    case tierTwo = "TIER_TWO"
//}
//
//struct Filters: Codable {
//}
//
//
//class RealmComic: Object {
//    @objc  dynamic var availabe: Int = 0
//    @objc private dynamic var collectionURI: String = ""
//     var items: List<String> = List<String>()
//    @objc  dynamic var returned: Int = 0
//    
//    static func newRealmChracterRootClass ( from root :Comic  ) -> RealmComic {
//        let newObject = RealmComic()
//        newObject.Availabel = root.available
//        newObject.CollectioUri = root.collectionURI
//        newObject.Items = root.items
//        newObject.Returned = root.returned
//        
//        return newObject
//    }
//    
//    
//    var Availabel: Int {
//        get{
//            return availabe
//        }
//        set{
//            safeWrite {
//                availabe = newValue
//            }
//        }
//    }
//    var CollectioUri: String {
//        get{
//            return collectionURI
//        }
//        set{
//            safeWrite {
//                collectionURI = newValue
//            }
//        }
//    }
//    var Items: [String] {
//        get{
//            return Array(items)
//        }
//        set{
//            safeWrite {
//                items.append(objectsIn: newValue)
//            }
//        }
//       
//    }
//    var Returned: Int {
//        get{
//            return availabe
//        }
//        set{
//            safeWrite {
//                returned = newValue
//            }
//        }
//        
//    }
//}
