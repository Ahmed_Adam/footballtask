//
//  RealmUrl.swift
//  Marvel
//
//  Created by Adam on 10/17/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import Foundation
import RealmSwift

class RealmUrl: Object{
    @objc  dynamic var type: String = ""
    @objc  dynamic var url: String = ""

}
