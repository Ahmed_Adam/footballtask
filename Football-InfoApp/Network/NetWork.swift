//
//  NetWork.swift
//  Football-InfoApp
//
//  Created by Adam on 11/17/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireImage

class WebServices{
    
    static let base = WebServices()
    
    let  baseUrl: String = "http://api.football-data.org/v2/"
    
    let apikey = "10997224357c42a38460f250b3b005f9"
    
    func getMethod (requestUrl : String , complition :   @escaping (_ error:Error? ,_ success: Bool , _ response  : String) -> Void){
        
        let urlwithPercentEscapes = baseUrl + requestUrl
        let url = URL(string : urlwithPercentEscapes)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        let headers = ["X-Auth-Token": apikey]
        request.allHTTPHeaderFields = headers
        //***** Alamofire request *****//
        Alamofire.request(request).responseJSON { (response ) in
            let jsonString = String(data: response.data!, encoding: String.Encoding.utf8)
            complition(nil, true, jsonString!)
        }
//        Alamofire.request(request).responseData { (response) in
//            switch response.result {
//            case .success( let data ):
//                let jsonString = data
//                //***** Data Returned *****//
//                let dataFromJson = try? JSONDecoder().decode(Football.self, from: jsonString)
//                complition(nil , true , dataFromJson!)
//            case .failure (let error):
//                print(error)
//            }        }
    }
    
    
    func getPhoto (url: String , complition :   @escaping (_ error:Error? ,_ success: Bool , _ photo: UIImage ) -> Void){
        let  imageUrl =  url


        Alamofire.request(imageUrl, method: .get).responseImage { response in
            print(response)
            guard let image = response.result.value else {
                // Handle error              
                return
            }
            // Do stuff with your image
            complition(nil,true , image)
        }
        
    }
    
}


extension String
{
    func encodeUrl() -> String
    {
        return self.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)!
    }
    
    
}

